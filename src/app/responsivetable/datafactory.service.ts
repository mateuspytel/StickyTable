import { Injectable } from '@angular/core';
import { NewModelData } from './data';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DatafactoryService {

  /**
   * Emits a mock observable filled with a number of 
   * random generated objects 
   * @param {number} quantity Quantity of mock objects
   */
  public getTableData(quantity: number): Observable<any> {
    return Observable.create( observer => {
      let container = [];
      for(let i=0; i<quantity; i++) {
        container.push(new NewModelData(i));
      }
      observer.next(container);
      observer.complete();
    });
  }

  constructor() { }

}
