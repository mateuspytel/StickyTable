import { Component, OnInit, OnDestroy, Input, HostListener } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { DatafactoryService } from './datafactory.service';
import { ResponsiveTable, TableCell, TableRow } from './ResponsiveTableOptions';
import { Observable } from 'rxjs/Observable';
import { query, stagger, style, animate, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-responsivetable',
  templateUrl: './responsivetable.component.html',
  styleUrls: ['./responsivetable.component.scss'],
  animations: [
    trigger('rowsAnimation', [
      transition('* => *', [
        query('.row', style({ transform: 'translateX(150%)' })),
        query('.row', stagger('80ms', [
          animate('100ms', style({ transform: 'translateX(0)' }))
        ]))
      ])
    ])
  ]
})
export class ResponsivetableComponent implements OnInit, OnDestroy {

  // Input data
  @Input() options$: Observable<ResponsiveTable>;

  // Input data subscription
  private optionsSubscription: Subscription;

  // Renderable sorted and prepared data from input observable
  _tableData: Array<TableRow> = [];
  
  // Maxium width of sticky column
  _stickyColumnWidth: string;
  
  // Maximum width of sticky column in pixels
  COLUMNFIXEDWIDTH = 60;

  _editColumn: number;

  makeEditable(index: number) {
    this._editColumn = index;
  }

  close(): void {
    this._editColumn = undefined;
  }

  constructor() {}

  ngOnInit() { 
    // Subscribe from input data
    this.optionsSubscription = this.options$.subscribe( data => {
      this._tableData = this.prepareStickyData(data.data);
      // Calculate sticky container width which is number of sticky columns multiplied
      // by a maximum sticky column width
      this._stickyColumnWidth = this.COLUMNFIXEDWIDTH * this._tableData[0].sticky_cell.length + 'px';
    });
  }

  private prepareStickyData(raw: Array<any>): any {
    const output_array: Array<TableRow> = [];
    raw.forEach( document => {                                                        // Iterate over input documents
      const row = new TableRow();                                                     // Create empty row container
      for (let key in document) {                                                     // Iterate over single document properties
        if (document[key] instanceof TableCell) {                                     // Check if property is actually TableCell object
          (document[key].options.sticky) ? row.sticky_cell.push(document[key])        // Check if column is sticky or not and append element to proper array
                                         : row.non_sticky_cell.push(document[key]);
        }
      }
      output_array.push(row);                                                         // Push row to output array
    });
    return output_array;                                                              // Return formated array
  }

  ngOnDestroy() {
    if (this.optionsSubscription) this.optionsSubscription.unsubscribe();
  }
}
