/**
 * Responsive table interface
 */
export class ResponsiveTable {
    title: string;                  // Table title
    data: Array<TableRow>;          // Input data
};

/** 
 * Single row interface 
 * Consists of two array properties - sticky cells which belongs to fixed columns
 * and non-sticky cells which will be normally scrollable 
 */
export class TableRow {
    sticky_cell: Array<TableCell>;
    non_sticky_cell: Array<TableCell>;
    constructor() { this.sticky_cell = []; this.non_sticky_cell = []; } 
}

/**
 * Single column cell - has decorated property of options which can modify cell behaviour
 */
export class TableCell {
    value: any;
    options: TableCellOptions
    constructor(value: any, options: TableCellOptions) {
        this.value = value;
        this.options = options;
    }
};

/**
 * Cell options
 */
export interface TableCellOptions {
    sticky?: boolean;
    wrap?: boolean;
    columnTitle: string;
}