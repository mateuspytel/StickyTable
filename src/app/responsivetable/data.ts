import { TableCell } from "./ResponsiveTableOptions";

/**
 * Mockup class which generates random information
 * on it's contstruction.
 * Properties are type of TableCell type - acceptable by 
 * ResponsiveTable
 */
export class NewModelData {
    id: TableCell;
    name: TableCell;
    subname: TableCell;
    desc: TableCell;
    date: TableCell;
    email: TableCell;
    location: TableCell;
    constructor(index: number) {
        this.id = new TableCell(Math.floor(Math.random() * 100), { sticky: true, columnTitle: 'ID'});
        this.name = new TableCell('Name ' + index, { columnTitle: 'NAME'});
        this.subname = new TableCell('Subname ' + index, { columnTitle: "SUBNAME"});
        this.desc = new TableCell('Example description of element with index: ' + index, { columnTitle: 'DESCRIPTION' });
        this.date = new TableCell(new Date().getTime(), { columnTitle: 'DATE'});
        this.email = new TableCell('example' + index + '@gmail.com', { columnTitle: 'E-MAIL'});
        this.location = new TableCell(LOCATIONS[Math.floor(Math.random() * LOCATIONS.length)], { columnTitle: 'LOCATION' });
    }
}

const LOCATIONS = [
    '421 E DRACHMAN TUCSON AZ 85705-7598 USA', 
    'SUITE 5A-1204 799 E DRAGRAM TUCSON AZ 85705 USA', 
    'POB 65502 TUCSON AZ 85728 USA'
];