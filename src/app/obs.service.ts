import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/interval';

export interface Response {
  key: string;
  value: string;
}

@Injectable()
export class ObsService {

  public publishObservable() : Observable<Response[]> {
    return Observable.create( observer => {
      let index = 0;
      setInterval( () => {
        observer.next( { key: ("KEY" + index), value: index * index });
        index++;
      }, 1000);
    })
  }

  constructor() { }

}
