import { Component, OnInit, OnDestroy } from '@angular/core';
import { ObsService, Response } from './obs.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { NewModelData } from './responsivetable/data';
import { DatafactoryService } from './responsivetable/datafactory.service';
import { TableCell, ResponsiveTable } from './responsivetable/ResponsiveTableOptions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'app';
  responseSubscription: Subscription;
  responseData: Array<NewModelData>;
  _tableOptions: Observable<ResponsiveTable>;
  
  constructor(private dataFactory: DatafactoryService) {}

  ngOnInit(): void {
    this.responseSubscription = this.dataFactory.getTableData(40).subscribe( data => {
      this.responseData = data;
      let table = new ResponsiveTable();
      table = new ResponsiveTable();
      table.data = data;
      table.title = 'Responsive data';
      this._tableOptions = Observable.create( observer => observer.next(table) );
    });
  }

  ngOnDestroy(): void {
    if (this.responseSubscription) {
      this.responseSubscription.unsubscribe();
    }
  }

}
