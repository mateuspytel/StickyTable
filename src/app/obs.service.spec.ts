import { TestBed, inject } from '@angular/core/testing';

import { ObsService } from './obs.service';

describe('ObsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ObsService]
    });
  });

  it('should be created', inject([ObsService], (service: ObsService) => {
    expect(service).toBeTruthy();
  }));
});
