import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ObsService } from './obs.service';
import { CommonModule } from '@angular/common';
import { ResponsivetableComponent } from './responsivetable/responsivetable.component';
import { DatafactoryService } from './responsivetable/datafactory.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClickoutsideDirective } from './clickoutside.directive';


@NgModule({
  declarations: [
    AppComponent,
    ResponsivetableComponent,
    ClickoutsideDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule
  ],
  providers: [ObsService, DatafactoryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
