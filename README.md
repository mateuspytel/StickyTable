# Sticky Table

Sticky table component in Angular 2. Responsive and highly customizeable table component in which selected columns can be optionally set as `sticky`.
Sticky columns are fixed at the left side of parent container. Component is a very convenient way of displaying large table data on mobile 
devices simultaneoulsy maintaing table navigation by locking columns during horizontal scroll.

## Installing

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Interfaces

Component accepts input data as a collection of `TableCell` objects.

### TableCell interface

```js 
class TableCell {
    value: any;
    options: TableCellOptions
};
```

### TableCellOptions interface
Table cell can be customizeable by decorating optional parameters - for example sticky paramter will fix the column to the left side

```js
interface TableCellOptions {
    sticky?: boolean;
    wrap?: boolean;
    columnTitle: string;
}
```

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.
